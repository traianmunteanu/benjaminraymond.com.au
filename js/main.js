function showHideMenu() {
  isMenuVisible = !isMenuVisible;
  var e = document.getElementById("divMenu");
  if (isMenuVisible) {
    $("#divMenu").slideUp()
  } else {
    $("#divMenu").slideDown()
  }
}
var isMenuVisible = true;
$(window).load(function() {
  $("#spinner").fadeOut("slow");
  $("#page").css({
    opacity: "1"
  })
});
$("img.lazy").show().lazyload({
  threshold: 800,
  effect: "fadeIn"
});
$(function() {
  $(".header-container ul a").click(function(e) {
    $.scrollTo($(this).attr("href"), 600, {
      easing: "swing",
      margin: true,
      offset: 25
    });
    e.preventDefault()
  });
  $("a.scroll-down").click(function(e) {
    $.scrollTo("#" + $(this).attr("rel"), 600, {
      easing: "swing",
      margin: true,
      offset: 25
    });
    e.preventDefault()
  });
  $("#gallery-content li a").click(function(e) {
    $.scrollTo("#gallery-content", 600, {
      easing: "swing",
      margin: true,
      offset: 25
    });
    e.preventDefault()
  })
});
(function(e) {
  e.each(["show", "hide"], function(t, n) {
    var r = e.fn[n];
    e.fn[n] = function() {
      this.trigger(n);
      return r.apply(this, arguments)
    }
  })
})($);
(function(e) {
  e.fn.extend({
    center: function(t) {
      var t = e.extend({
        inside: window,
        transition: 0,
        minX: 0,
        minY: 0,
        withScrolling: true,
        vertical: true,
        horizontal: true
      }, t);
      return this.each(function() {
        var n = {
          position: "absolute"
        };
        if (t.vertical) {
          var r = (e(t.inside).height() - e(this).outerHeight()) / 2;
          if (t.withScrolling) r += e(t.inside).scrollTop() || 0;
          r = r > t.minY ? r : t.minY;
          e.extend(n, {
            top: r + "px"
          })
        }
        if (t.horizontal) {
          var i = (e(t.inside).width() - e(this).outerWidth()) / 2;
          if (t.withScrolling) i += e(t.inside).scrollLeft() || 0;
          i = i > t.minX ? i : t.minX;
          e.extend(n, {
            left: i + "px"
          })
        }
        if (t.transition > 0) e(this).animate(n, t.transition);
        else e(this).css(n);
        return e(this)
      })
    }
  })
})($);
$(window).resize(function() {
  var e = $(window).height() - 159;
  $(".container").height(e);
  var t = $(window).height() - 50;
  if ($(this).width() > 1140) {
    $(".container-page").height(t)
  } else if ($(this).width() - 15 >= 768) {
    $(".container-page").css("height", "361px")
  } else {
    $(".container-page").css("height", "848px")
  }
  $(".lightbox").hide();
  $(".lightboxOverlay").hide()
});
$(document).ready(function() {
  var e = $(window).height() - 159;
  $(".container").height(e);
  if ($(window).width() > 1140) {
    var t = $(window).height() - 50;
    $(".container-page").height(t)
  } else {
    $(".container-page").height();
    $("#gallery-content a").click(function(e) {
      e.preventDefault()
    })
  }
  $("a.enquiry-link").click(function(e) {
    e.preventDefault();
    $("#enquiry").show("slow", function() {
      $("#enquiry div.hidden").fadeIn("slow")
    })
  });
  $("a.enquiry-close").click(function(e) {
    e.preventDefault();
    $("#enquiry div.hidden").fadeOut("slow", function() {
      $("#enquiry").hide("slow")
    })
  });
  var n = $("#story-content");
  var r = n.outerHeight();
  $(document).scroll(function(e) {
    var t = (r - window.scrollY) / (r * 4);
    if (t >= 0) {
      n.css("opacity", 1 - t * 4)
    } else {
      n.css("opacity", "1")
    }
  });
  $("#next").click(function(e) {
    e.preventDefault();
    $(".text_container").fadeOut();
    $(".container").backstretch("next")
  });
  $("#prev").click(function(e) {
    e.preventDefault();
    $(".text_container").fadeOut();
    $(".container").backstretch("prev")
  });
  $(window).on("backstretch.before", function(e, t) {
    $(".text_container").fadeOut()
  });
  $("#submit_btn").click(function() {
    var e = $("input[name=name]").val();
    var t = $("input[name=email]").val();
    var n = $("input[name=phone]").val();
    var r = $("textarea[name=message]").val();
    var i = true;
    if (e == "") {
      $("input[name=name]").css("border-color", "red");
      i = false
    }
    if (t == "") {
      $("input[name=email]").css("border-color", "red");
      i = false
    }
    if (n == "") {
      $("input[name=phone]").css("border-color", "red");
      i = false
    }
    if (r == "") {
      $("textarea[name=message]").css("border-color", "red");
      i = false
    }
    if (i) {
      post_data = {
        userName: e,
        userEmail: t,
        userPhone: n,
        userMessage: r
      };
      $.post("_contact.php", post_data, function(e) {
        $("#enquiry h2").html("Thank you");
        $("#result").hide();
        $("#contact_form").html('<div class="success"><p>' + e + '</p><button class="close_btn">ok</button></div>').slideDown();
        $(".close_btn").click(function(e) {
          e.preventDefault();
          $("#enquiry").hide("fast")
        });
        var t = $(window).width();
        if (t < 768) {} else {
          $("#enquiry").height(300)
        }
      }).fail(function(e) {
        $("#result").hide().html('<div class="error"><p>' + e.statusText + "</p></div>").slideDown()
      })
    }
  });
  $("#contact_form input, #contact_form textarea").keyup(function() {
    $("#contact_form input, #contact_form textarea").css("border-color", "");
    $("#result").slideUp()
  })
})